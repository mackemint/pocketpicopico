pico-8 cartridge // http://www.pico-8.com
version 33
__lua__
--pocketpico visualizer
function _init()
 _max_carts=35 --total carts
 cartdata("pocketpico_12")
 _dndx=63 --persistant counter index
 _min=5   --min play time
 _rnd=15  --max play time
 _start = flr(rnd(_max_carts)) --what cart to start with
 _cnt=dget(_dndx)+1 --get counter
 _played ={} --list of played carts
end

function run_cart(_cart)
 dset(_dndx, _cnt)  --store counter
 dset(_cnt, _start) --store last played
 _run_time=flr(rnd(_rnd))+_min
 load(_cart,"",_run_time)
end

function has_value (tab, val)
 for index, value in ipairs(tab) do
  if value == val then
   return true
  end
 end
 return false
end

function _update()
 for i=1,_cnt,1 do --populate played list
  _played[i]=dget(i)
 end
 _played[_cnt]=_start
 while has_value(_played,_start) do
  print(start)
  _start = flr(rnd(_max_carts))
 end
 if _cnt==_max_carts then
  _played ={} --empty played list
  dset(0,dget(_cnt))--save last
  _cnt=-1
  for i=2, _dndx+1, 1 do
   dset(i,0) --empty persistant played list
  end
 end
 if _start == 0 then
  run_cart("symmetry-0.p8")
 elseif _start == 1 then
  run_cart("neon-0.p8")
 elseif _start == 2 then
  run_cart("flywin.p8")
 elseif _start == 3 then
  run_cart("qix.p8")
 elseif _start == 4 then
  run_cart("pollocksim.p8")
 elseif _start == 5 then
  run_cart("genetic.p8")
 elseif _start == 6 then
  run_cart("music_visualizer-1.p8")
 elseif _start == 7 then
  run_cart("podofimose-0.p8")
 elseif _start == 8 then
  run_cart("word_quiz_animals-7.p8")
 elseif _start == 9 then
  run_cart("attractor.p8")
 elseif _start == 10 then
  run_cart("nazubukiho-0.p8")
 elseif _start == 11 then
  run_cart("sss-0.p8")
 elseif _start == 12 then
  run_cart("zinmtamu-0.p8")
 elseif _start == 13 then
  run_cart("bunny_envmap-0.p8")
 elseif _start == 14 then
  run_cart("orbys-0.p8")
 elseif _start == 15 then
  run_cart("fugihiruhu-0.p8")
 elseif _start == 16 then
  run_cart("piconiccc-1.p8")
 elseif _start == 17 then
  run_cart("rainbowtv-0.p8")
 elseif _start == 18 then
  run_cart("snekinthewater-0.p8")
 elseif _start == 19 then
  run_cart("waves0.p8")
 elseif _start == 20 then
  run_cart("waves1.p8")
 elseif _start == 21 then
  run_cart("waves2.p8")
 elseif _start == 22 then
  run_cart("waves3.p8")
 elseif _start == 23 then
  run_cart("waves4.p8")
 elseif _start == 24 then
  run_cart("waves5.p8")
 elseif _start == 25 then
  run_cart("waves6.p8")
 elseif _start == 26 then
  run_cart("waves7.p8")
 elseif _start == 27 then
  run_cart("waves8.p8")
 elseif _start == 28 then
  run_cart("pico_1k_jam_thanks-0.p8")
 elseif _start == 29 then
  run_cart("fresh_water-1.p8")
 elseif _start == 30 then
  run_cart("pico_1k_jam_invitation-0.p8")
-- elseif _start == 31 then -- hangs and doesnt progress past this ;(
--  run_cart("fesi.p8")
 elseif _start == 32 then
  run_cart("heytherethelr.p8")
 elseif _start == 33 then
  run_cart("lotsocolors.p8")
 else
  run_cart("lorenz3d-1.p8")
 end
end
__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000005500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000005550050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000554555550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700666555550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000005556650000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000005050050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
