# pocketpicopico

Visualizer project using PocketCHIP, Samsung Pico projector and Pico-8

The cart pocketpico loads a random other cart for some time before exiting and selecting another.
Run with pocketpicopico.sh
Put the *.desktop files in ~/.config/autostart/ to auto start and disable screen saver.
